#!/bin/bash

ipfile=~/.currentip
msgfile=/tmp/ipmonitor.txt
email="mlopezviedma@gmail.com"
[ -f $ipfile ] && lastip="$(cat $ipfile)"
currentip="$(curl -s ifconfig.me)"
[ -z "$currentip" ] && exit 1

if [ "$lastip" != "$currentip" ]; then
	echo "IP anterior: ${lastip}" > $msgfile
	echo "IP nueva: ${currentip}" >> $msgfile
	cat $msgfile | mailx -s "Cambio de IP BVC Online" "$email"
	echo "$currentip" > $ipfile
	rm -f $msgfile
fi

